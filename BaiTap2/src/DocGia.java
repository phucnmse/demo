
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class DocGia {
    //properties
    String tenDocGia;
    String diachiDocGia;
    //methods
        // constructor: là special methods để khởi tạo đối tượng với data gì đó
        // đây là constructor chuẩn - default constructor 
    public DocGia()
    {
        tenDocGia="ABCD";
        diachiDocGia="BCDE";
        
    }
    //constructor with parameters 
    public DocGia(String a,String b)
    {
        tenDocGia=a;
        diachiDocGia=b;
        
    }
    void nhapDocGia()
    {
        Scanner tool = new Scanner(System.in);
        System.out.println("Nhập tên độc giả :");
        tenDocGia=tool.nextLine();
        System.out.println("Nhập địa chỉ độc giả:");
        tool= new Scanner (System.in);
        diachiDocGia=tool.nextLine();
        
        
    }
    void xuatDocGia()
   {
            System.out.println("Tên:"+tenDocGia);
            System.out.println("Địa chỉ:"+diachiDocGia);
            
   }
    public static void main(String []args)
    {
       // Sach x=new Sach();
       // x.nhapSach();
      //  x.xuatSach();
        
        
       // DocGia t=new DocGia();
       // t.nhapDocGia();
       // t.xuatDocGia();
        String tenDocGiaExCel="BK010";
        String diachiDocGiaExcel="Han va Yeu";
       
        DocGia s=new DocGia(tenDocGiaExCel,diachiDocGiaExcel);
        s.xuatDocGia();
        
    }

    }
