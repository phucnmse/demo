/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A;

/**
 *
 * @author ADMIN
 */
public class Dog {
    public int id;
    private String sex;// che dau du lieu 
    protected String hairColor;// class con thấy được , cùng package thấy lun 
    int age;//default 
    
    //default constructor

    public Dog(int id, String sex, String hairColor, int age) {
        this.id = id;
        this.sex = sex;
        this.hairColor = hairColor;
        this.age = age;
    }

    public Dog() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    void output()
    {
        System.out.println(id+"-"+sex);
    }
    private void update()
    {
        System.out.println("Đây là hàm update");
    }
    public final void UpgradeDog()
    {
        System.out.println("Tăng cân");
    }
    public void saleDog()
    {
        System.out.println("Mại dzô");
    }
    protected void discountDog()
    {
        System.out.println("Disccount 10%");
    }
    
    
}
