/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package B;
import A.Dog;

/**
 *
 * @author ADMIN
 */
public class PhuQuocDog extends Dog{
    String name;
    public PhuQuocDog()
    {
        id=1;
       name="Phu";
       hairColor="black";
       
    }

    public PhuQuocDog(int id, String name, String hairColor)
    {
        this.name=name;
        this.id=id;
        this.hairColor=hairColor;
    }
    //output của lớp cha default chỉ xài trong gói , lớp con khác gói nên ko thể gọi là override được 
   public void output()
    {
        System.out.println("id:"+id);
        System.out.println("name:"+name);
        System.out.println("hair color:"+getHairColor());
        
    }
   
    
}
